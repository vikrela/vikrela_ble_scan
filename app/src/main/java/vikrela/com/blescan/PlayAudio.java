package vikrela.com.blescan;

/**
 * Created by Harsh on 6/18/2017.
 */

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class PlayAudio extends Service {
    private static final String TAG = null;
    MediaPlayer player;

    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.ic_beep_notif);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        if (player.isLooping() != true) {

        }
        return 1;
    }

    public void onStop() {
        player.stop();
        player.release();
    }

    public void onPause(){
        player.stop();
        player.release();
    }

    public void onDestroy(){
        player.stop();
        player.release();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
