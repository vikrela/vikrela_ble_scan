package vikrela.com.blescan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Harsh on 6/18/2017.
 */

public class ListAdapter_BTLE_Devices extends BaseAdapter {
    Context context;
    List<BTLE_Device> devices;
    int flag1 = 0, flag2 = 0, flag3 = 0, flag4 = 0;

    public ListAdapter_BTLE_Devices(Context context, List<BTLE_Device> objects) {
        this.context = context;
        devices = objects;
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int position) {
        return devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.listview_custom_row, parent, false);
        final BTLE_Device btle_device = devices.get(position);
        String name = btle_device.getName();
        String address = btle_device.getAddress();
        int rssi = btle_device.getRssi();

        View view = convertView.findViewById(R.id.view1);
        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        if (name != null && name.length() > 0) {
            txtName.setText(btle_device.getName());
        } else {
            txtName.setText("No Name");
        }

        TextView txtRssi = (TextView) convertView.findViewById(R.id.txtRssi);
        txtRssi.setText("RSSI:" + Integer.toString(rssi));
        TextView txtAddress = (TextView) convertView.findViewById(R.id.txtAddress);
        if (address != null && address.length() > 0) {
            txtAddress.setText(btle_device.getAddress());
        } else {
            txtAddress.setText("No Address");
        }
        if (rssi < 0 && rssi >= -70) {
            convertView.setVisibility(View.VISIBLE);
            view.setBackgroundColor(Color.GREEN);
            Intent intent = new Intent(context, PlayAudio.class);
            /*if (btle_device.getAddress().equals("66:55:44:33:22:11") || btle_device.getAddress().equals("66:55:44:33:22:01")) {
                context.startService(intent);
            }*/
        }
        if (rssi < -70 && rssi >= -85) {
            convertView.setVisibility(View.VISIBLE);
            view.setBackgroundColor(Color.YELLOW);
            Intent intent = new Intent(context, PlayAudio.class);
            if (btle_device.getAddress().equals("66:55:44:33:22:11") && flag1 == 0) {
                context.startService(intent);
                flag1 += 1;
                final MaterialDialog dialog = new MaterialDialog.Builder(context)
                        .progress(true, 0)
                        .show();
                StringRequest request = new StringRequest(Request.Method.POST, "http://kkwagheducationsocietyalumni.in/Vikrela/Android/ProximityScan.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.d("Response:::", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if (message.equals("added")) {
                                Toast.makeText(context, "Hawker Addded", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error:::", error.toString());
                        dialog.dismiss();
                        Toast.makeText(context, "Hawker Not Here yet", Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("macAddress", btle_device.getAddress());
                        params.put("deviceName", btle_device.getName());
                        params.put("status", "In");
                        return params;
                    }
                };
                AppController.getInstance().addToRequestQueue(request);
            }
            if (btle_device.getAddress().equals("66:55:44:33:22:01") && flag2 == 0) {
                context.startService(intent);
                flag2 += 1;
                final MaterialDialog dialog = new MaterialDialog.Builder(context)
                        .progress(true, 0)
                        .show();
                StringRequest request = new StringRequest(Request.Method.POST, "http://kkwagheducationsocietyalumni.in/Vikrela/Android/ProximityScan.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.d("Response:::", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if (message.equals("added")) {
                                Toast.makeText(context, "Hawker Addded", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error:::", error.toString());
                        dialog.dismiss();
                        Toast.makeText(context, "Hawker Not Here yet", Toast.LENGTH_SHORT).show();
                    }
                }) {
                        @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("macAddress", btle_device.getAddress());
                        params.put("deviceName", btle_device.getName());
                        params.put("status", "In");
                        Log.d("Params:::", params.toString());
                        return params;
                    }
                };
                AppController.getInstance().addToRequestQueue(request);
            }
        }
        if (rssi < -85 && rssi >= -92) {
            convertView.setVisibility(View.VISIBLE);
            view.setBackgroundColor(Color.RED);
            if (btle_device.getAddress().equals("66:55:44:33:22:01") && flag3 == 0) {
                flag3 += 1;
                final MaterialDialog dialog = new MaterialDialog.Builder(context)
                        .progress(true, 0)
                        .content("Updating...")
                        .show();
                StringRequest request = new StringRequest(Request.Method.POST, "http://kkwagheducationsocietyalumni.in/Vikrela/Android/ProximityScan.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.d("Response:::", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if (message.equals("added")) {
                                Toast.makeText(context, "Hawker Gone", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error:::", error.toString());
                        dialog.dismiss();
                        Toast.makeText(context, "Hawker Not Here yet", Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("macAddress", btle_device.getAddress());
                        params.put("deviceName", btle_device.getName());
                        params.put("status", "Out");
                        return params;
                    }
                };
                AppController.getInstance().addToRequestQueue(request);

            }
            if (btle_device.getAddress().equals("66:55:44:33:22:11") && flag4 == 0) {
                flag4 += 1;
                final MaterialDialog dialog = new MaterialDialog.Builder(context)
                        .progress(true, 0)
                        .content("Updating...")
                        .show();
                StringRequest request = new StringRequest(Request.Method.POST, "http://kkwagheducationsocietyalumni.in/Vikrela/Android/ProximityScan.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.d("Response:::", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if (message.equals("added")) {
                                Toast.makeText(context, "Hawker Gone", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error:::", error.toString());
                        dialog.dismiss();
                        Toast.makeText(context, "Hawker Not Here yet", Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("macAddress", btle_device.getAddress());
                        params.put("deviceName", btle_device.getName());
                        params.put("status", "Out");
                        return params;
                    }
                };
                AppController.getInstance().addToRequestQueue(request);

            }
        } /*else if (rssi < -92 && rssi >= -99) {
            convertView.setVisibility(View.GONE);
            view.setBackgroundColor(Color.RED);
            Intent intent = new Intent(context, PlayAudio.class);
            context.stopService(intent);*//*
        } */ /*else {
            convertView.setVisibility(View.GONE);
        }*/
        return convertView;
    }
}

